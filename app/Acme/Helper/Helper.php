<?php
namespace Acme\Helper;

class Helper
{
    /**
     * Replace old key with new key
     *
     * @param $array
     * @param $oldKey
     * @param $alternativeKey
     *
     * @return mixed
     * @internal param $all
     *
     */
    public function keyReplacement( $array, $oldKey, $alternativeKey )
    {
        foreach( $array as $key => $value )
        {
            if( array_key_exists( $oldKey, $array[ $key ] ) )
            {
                $array[ $key ] [ $alternativeKey ] = $array[ $key ] [ $oldKey ];
                unset( $array[ $key ][ $oldKey ] );
            }
        }

        return $array;
    }

    /**
     * Fetch identify number from url;
     *
     * @param $string
     *
     * @return int|string
     */
    public function identifyCodeBrand( $string )
    {
        $identifyCode = 0;
        preg_match_all( '/\d/', $string, $output );
        for( $i = 0; $i < count( $output ); $i++ )
        {
            for( $j = 0; $j < count( $output[ $i ] ); $j++ )
            {
                $identifyCode .= $output[ $i ][ $j ];
            }
        }

        return $identifyCode;
    }

    /**
     * Convert 3depth array to flat array.
     * uses in fetching data from brand
     *
     * @param $response
     *
     * @return array
     */
    function flatten( $response )
    {
        foreach( $response as $value )
        {
            foreach( $value as $value1 )
            {
                $all[] = $value1;
            }
        }

        return $all;
    }

}