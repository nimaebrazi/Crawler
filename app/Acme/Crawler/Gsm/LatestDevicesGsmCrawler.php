<?php
namespace Acme\Crawler\Gsm;

use Acme\Helper\Helper as Helper;

class LatestDevicesGsmCrawler extends GsmCrawler
{
    /**
     * @return mixed
     */
    public function repair()
    {
        $respond = $this->respond();

        return $this->helpers->keyReplacement( $this->generateArrayLogic( $respond ), '0', 'ImageAddress' );
    }

    /**
     * @param $respond
     *
     * @return mixed
     */
    protected function generateArrayLogic( $respond )
    {
        foreach( $respond as $key => $value )
        {
            switch( $value[ 'nodeName' ] )
            {
                case 'h4':
                    $all[ $key ][ 'title' ] = $respond[ $key ][ 'text' ];
                    break;
                case 'a':
                    $all[ $key ] = [
                        'Device' => $value[ 'text' ],
                        'link' => $value[ 'href' ],
                    ];
                    break;
                case 'img':
                    array_push( $all[ $key - 1 ], $value[ 'src' ] );
                    break;
            }
        }

        return $all;
    }
}