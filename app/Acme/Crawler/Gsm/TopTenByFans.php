<?php
namespace Acme\Crawler\Gsm;

use Acme\Helper\Helper as Helper;

class TopTenByFans extends GsmCrawler
{

    /**
     * @return mixed
     */
    public function repair()
    {
        $respond = $this->respond();

        return $this->helpers->keyReplacement( $this->generateArrayLogic( $respond ), '0', 'Favorite' );

    }

    /**
     * @param $respond
     *
     * @return mixed
     */
    protected function generateArrayLogic( $respond )
    {
        $number = 1;
        foreach( $respond as $key => $value )
        {
            switch( $value[ 'nodeName' ] )
            {
                case 'h4':
                    $all [ $key ][ 'title' ] = $value[ 'text' ];
                    break;
                case 'a':
                    $all[ $key ] = [
                        'Number' => $number,
                        'Device' => $value[ 'text' ],
                        'Address' => $value[ 'href' ]
                    ];
                    $number++;
                    break;
                case 'td':
                    array_push( $all[ $key - 2 ], $value[ 'text' ] );
                    break;
            }
        }

        return $all;
    }
}