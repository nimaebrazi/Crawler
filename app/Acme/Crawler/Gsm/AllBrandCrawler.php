<?php
namespace Acme\Crawler\Gsm;


class AllBrandCrawler extends GsmCrawler
{


    /**
     * @return mixed
     */
    public function repair()
    {
        $respond = $this->respond();

        unset( $respond[ 0 ], $respond[ 1 ], $respond[ 2 ] );

        $all = $this->helpers->keyReplacement( $this->generateArrayLogic( $respond ), '0', 'ImageAddress' );

        unset( $respond );

        return $all;
    }

    /**
     * @param $respond
     *
     * @return mixed
     * @internal param $all
     * @internal param $id
     *
     */
    protected function generateArrayLogic( $respond )
    {
        $id = 1;
        foreach( $respond as $key => $value )
        {
            switch( $value[ 'nodeName' ] )
            {
                case 'a':
                    $all[ $key ] = [
//                        'Id' => $id,
                        'IdentifyCode' => $this->helpers->identifyCodeBrand( $value[ 'href' ] ),
                        'Brand' => $value[ 'text' ],
                        'Address' => $value[ 'href' ]
                    ];
                    $id++;
                    break;
                case 'img':
                    array_push( $all[ $key - 1 ], $value[ 'src' ] );
                    break;
            }
        }

        return $all;
    }



}