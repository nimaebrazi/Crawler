<?php
namespace Acme\Crawler\Gsm;

use Acme\Helper\Helper;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

abstract class GsmCrawler
{
    /**
     * @var
     */
    protected $url;
    /**
     * @var
     */
    protected $method;
    /**
     * @var
     */
    protected $crawler;
    /**
     * @var
     */
    protected $client;
    /**
     * @var
     */
    protected $cssSelector;
    /**
     * @var
     */
    protected $xPath;
    /**
     * @var string
     */
    protected $gsmUrl = 'http://gsmarena.com/';
    /**
     * @var string
     */
    protected $mobileGsmUrl = 'http://m.gsmarena.com/';
    /**
     * @var Helper
     */
    protected $helpers;
    /**
     * @var
     */
    protected $respond;


    /**
     * GsmCrawler constructor.
     *
     * @param $method
     * @param $url
     */
    public function __construct( $method = 'GET' )
    {
        $this->method = $method;
        $this->client = new Client();
        $this->helpers = new Helper();
    }

    /**
     * @return mixed
     */
    abstract public function repair();

    /**
     * @param $respond
     *
     * @return mixed
     */
    abstract protected function generateArrayLogic( $respond );

    /**
     * send all crawl page with selector in array.
     * respond must be pass to repair for generate all data needed.
     * @return array
     */
    public function respond()
    {
        return $this->getCrawler()->filter( $this->getCssSelector() )->each( $this->response() );
    }

    /**
     * Fetch all data from html tags with a symphony/crawler in closure.
     * @return \Closure
     */
    public function response()
    {
        return function( Crawler $nodes )
        {
            $response[ 'nodeName' ] = $nodes->nodeName();
            $response[ 'text' ] = $nodes->text();
            $response[ 'href' ] = $this->gsmUrl . $nodes->attr( 'href' );
            $response[ 'src' ] = $nodes->attr( 'src' );

            return $response;
        };
    }

    /**
     * Return data with json encode.
     * @return string
     * @internal param string $array
     *
     */
    public function toJson()
    {
        return json_encode( array_values( $this->repair() ), ( JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES |
            JSON_NUMERIC_CHECK | JSON_PRESERVE_ZERO_FRACTION ) );
    }

    /**
     * Return all fetched data from repair in an array.
     * @return mixed
     */
    public function toArray()
    {
        return $this->repair();
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getCrawler()
    {
        return $this->crawler;
    }

    /**
     * @param $url
     *
     * @return $this
     */
    public function setUrl( $url )
    {
        $this->url = $url;
        $this->crawler = $this->client->request( $this->getMethod(), $this->getUrl() );

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public function getCssSelector()
    {
        return $this->cssSelector;
    }

    /**
     * @return string
     */
    public function getGsmUrl()
    {
        return $this->gsmUrl;
    }

    /**
     * @param mixed $cssSelector
     *
     * @return $this
     */
    public function setCssSelector( $cssSelector )
    {
        $this->cssSelector = $cssSelector;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getXPath()
    {
        return $this->xPath;
    }

    /**
     * @param mixed $xPath
     *
     * @return $this
     */
    public function setXPath( $xPath )
    {
        $this->xPath = $xPath;

        return $this;
    }


}
