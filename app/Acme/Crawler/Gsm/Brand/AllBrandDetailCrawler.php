<?php
namespace Acme\Crawler\Gsm\Brand;

use Acme\Crawler\Gsm\GsmCrawler;
use medoo;


class AllBrandDetailCrawler extends GsmCrawler
{

    /*
     * ToDo:
     * when user pass @var brand . we must be fetch identify code from database.
     * any ways now send to pageCount temporary.
     * change pageCount structure when design db
     * */


    const FILTER = 'f';
    const PAGE = 'p';
    const PHONES = 'phones';
    private $brand;




    /**
     * @return mixed
     */
    public function repair()
    {
        $respond = $this->respond();
        $all = $this->helpers->keyReplacement( $this->generateArrayLogic( $respond ), '0', 'ImageAddress' );

        return $all;
    }

    /**
     * @param $respond
     *
     * @return mixed
     */
    protected function generateArrayLogic( $respond )
    {
        foreach( $respond as $key => $value )
        {
            switch( $value[ 'nodeName' ] )
            {

                case 'a':
                    $all[ $key ] = [
                        'Device' => $value[ 'text' ],
                        'link' => $value[ 'href' ]
                    ];
                    break;
                case 'img':
                    array_push( $all[ $key - 1 ], $value[ 'src' ] );
                    break;
            }
        }

        return $all;
    }

    public function setBrand( $brand )
    {
        $this->brand = $brand;

        return $this;
    }

    private function findIdentifyCode()
    {
        $database = new medoo( [
            'database_type' => 'mysql',
            'database_name' => 'gsm',
            'server' => 'localhost',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
        ] );

        $code = $database->select( 'brands', 'identify_code', ['name' => $this->getBrand()] );

        return $code[ 0 ];
    }

    /**
     * @return mixed
     */
    public function getPageCount()
    {
        $crawler = new PageCount();
        $pageCount = (int)$crawler->setUrl( $this->generateBrandUrl() . '1' . '.php' )
            ->setCssSelector( '.nav-pages strong , .nav-pages a' )
            ->toArray();

        if( $pageCount == 0 )
        {
            return $pageCount = $pageCount + 1;
        }

        return $pageCount;
    }

    /**
     * @return mixed
     */
    private function getBrand()
    {
        return $this->brand;
    }

    private function generateBrandUrl()
    {
        return $this->gsmUrl . $this->getBrand() . '-phones-f-' . $this->findIdentifyCode() . '-0-p';
    }

    public function crawlBrand()
    {
        $pageCount = $this->getPageCount();
        print $pageCount;
        $crawler = new AllBrandDetailCrawler();

        $response = [];
        if( $pageCount == 1 )
        {
            return $response = $crawler->setUrl( $this->generateBrandUrl() . '1' . '.php' )
                ->setCssSelector( '#review-body a , #review-body img , #review-body strong' )->toArray();
        }
        else
        {
            for( $page = 1; $page <= $pageCount; $page++ )
            {
                $result = $crawler->setUrl( $this->generateBrandUrl() . $page . '.php' )
                    ->setCssSelector( '#review-body a , #review-body img , #review-body strong' )->toArray();
                array_push( $response, $result );
            }

        }

        return $this->helpers->flatten( $response );
    }


    /**
     * Return data with json encode.
     * @return string
     * @internal param string $array
     *
     */
    public function toJson()
    {
        return json_encode( array_values( $this->crawlBrand() ), ( JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES |
            JSON_NUMERIC_CHECK | JSON_PRESERVE_ZERO_FRACTION ) );
    }

    /**
     * Return all fetched data from repair in an array.
     * @return mixed
     */
    public function toArray()
    {
        return $this->crawlBrand();
    }
}