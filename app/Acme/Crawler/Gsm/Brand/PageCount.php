<?php
/**
 * Created by PhpStorm.
 * User: nima
 * Date: 3/16/16
 * Time: 6:07 AM
 */

namespace Acme\Crawler\Gsm\Brand;


use Acme\Crawler\Gsm\GsmCrawler;

class PageCount extends GsmCrawler
{
    private $pageCount;

    /**
     * @return mixed
     */
    public function repair()
    {
        return $this->generateArrayLogic( $this->respond() );
    }

    /**
     * @param $respond
     *
     * @return mixed
     */
    protected function generateArrayLogic( $respond )
    {
        foreach( $respond as $key => $value )
        {
            switch( $value[ 'nodeName' ] )
            {
                case 'a':
                    $pageCount = $value[ 'text' ];
                    break;
            }
        }

        return $pageCount;
    }

    /**
     * @return mixed
     */
    public function getPageCount()
    {
        return $this->pageCount;
    }
}